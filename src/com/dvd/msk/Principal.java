package com.dvd.msk;

import com.dvd.msk.gui.VentanaController;
import com.dvd.msk.gui.VentanaModel;

/**
 * Created by k3ym4n on 03/12/2015.
 */
public class Principal {

    public static void main (String args[]){

        Ventana view = new Ventana();
        VentanaModel model = new VentanaModel();
        VentanaController controller=new VentanaController(view,model,view.getObjBotonera());
    }
}
