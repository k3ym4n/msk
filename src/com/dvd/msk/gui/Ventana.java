package com.dvd.msk.gui;

import com.dvd.msk.base.Cancion;
import com.dvd.msk.base.Grupo;
import com.dvd.msk.gui.Botonera;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * Created by k3ym4n on 03/12/2015.
 */
public class Ventana {
     public JTabbedPane Panelprincipal;
     public JPanel Pgrupo;
     public JTextField tfgruponombre;
     public JTextField tfgrupogenero;
     public JTextField tfgruponacionalidad;
     public JScrollPane splistagrupos;
     public JList JlistaGrupos;
     public JList <Grupo>listagrupodiscos;
     public JList <Cancion>listagrupocanciones;
     public JTextField tfgrupobuscar;
     public JDateChooser dategrupofinicio;
     public JDateChooser dategrupoffin;
     public JPanel Pdisco;
     public JComboBox combodiscogrupo;
     public JTextField tfdiscotitulo;
     public JTextField floatdiscoduracion;
     public JTextField tfdiscoproductora;
     public JTextField intdisconumcanciones;
     public JTextField floatdiscoprecio;
     public JList JlistaDiscos;
     public JList listadiscocanciones;
     public JTextField tfdiscobuscar;
     public JPanel Pcancion;
     public JComboBox combocanciongrupo;
     public JTextField tfcanciontitulo;
     public JTextField floatcancionduracion;
     public JTextField intcanciontrack;
     public JTextField floatcancionprecio;
     public JTextField tfcanciongenero;
     public JList JlistaCanciones;
     public JComboBox combocanciondisco;
     public JTextField tfcancionbuscar;
    //Objeto botonera
     public Botonera objBotonera;
     private JPanel TODO;


    public Ventana(){
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(TODO);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public Botonera getObjBotonera() {
        return objBotonera;
    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}

