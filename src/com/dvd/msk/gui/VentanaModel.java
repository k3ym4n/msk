package com.dvd.msk.gui;

import com.dvd.msk.Ventana;
import com.dvd.msk.base.Cancion;
import com.dvd.msk.base.Disco;
import com.dvd.msk.base.Grupo;
import com.dvd.msk.util.Constantes;
import org.w3c.dom.DOMImplementation;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import javax.swing.text.Element;


/**
 * Created by k3ym4n on 03/12/2015.
 */
//que es extends components
public class VentanaModel extends Component implements Serializable{

    public ArrayList<Grupo> listaGrupos;
    public ArrayList<Disco> listaDiscos;
    public ArrayList<Cancion> listaCanciones;

    public VentanaModel(){


        File FGrupos = new File (Constantes.PATHGrupos);
        if(FGrupos.exists()){
            listaGrupos = (ArrayList<Grupo>)cargarFicheroGrupo();
        }else{
            listaGrupos = new ArrayList<Grupo>();
        }


        File FDiscos = new File (Constantes.PATHDiscos);
        if(FDiscos.exists()){
            listaDiscos = (ArrayList<Disco>)cargarFicheroDisco();
        }else{
            listaDiscos = new ArrayList<Disco>();
        }

        File FCanciones = new File (Constantes.PATHCanciones);
        if(FCanciones.exists()){
            listaCanciones = (ArrayList<Cancion>)cargarFicheroCancion();
        }else{
            listaCanciones = new ArrayList<Cancion>();
        }
    }
    public void guardarFicheroCancion(){
        ObjectOutputStream obj=null;

        try {
            obj= new ObjectOutputStream(new FileOutputStream(Constantes.PATHCanciones));
            obj.writeObject(listaCanciones);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Object cargarFicheroCancion() {
        Object objeto=null;

        ObjectInputStream obj = null;
        try {
            obj = new ObjectInputStream(new FileInputStream(Constantes.PATHCanciones));
            objeto = obj.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        return objeto;
    }

    public void guardarFicheroDisco(){
        ObjectOutputStream obj = null;

        try {
            obj=new ObjectOutputStream(new FileOutputStream(Constantes.PATHDiscos));
            obj.writeObject(listaDiscos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Object cargarFicheroDisco() {
        Object objeto = null;
        try {
        ObjectInputStream obj = new ObjectInputStream(new FileInputStream(Constantes.PATHDiscos));
            objeto=obj.readObject();
        } catch (IOException ioe) {
           ioe.printStackTrace();
        } catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        return objeto;
    }
    public void guardarFicheroGrupo(){
        ObjectOutputStream obj = null;
        try {
            obj = new ObjectOutputStream(new FileOutputStream(Constantes.PATHGrupos));
            obj.writeObject(listaGrupos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Object cargarFicheroGrupo() {
        Object objeto = null;
        try {
            ObjectInputStream obj = new ObjectInputStream(new FileInputStream(Constantes.PATHGrupos));
            objeto = obj.readObject();

        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return objeto;
    }
    public void guardarGrupo(Grupo grupo){
        System.out.println(grupo);
        listaGrupos.add(grupo);

    }
    public void eliminarGrupo(int posicion){
        listaGrupos.remove(posicion);
    }
    public void guardarDisco(Disco disco){
        listaDiscos.add(disco);
    }
    public void eliminarDisco(int posicion){
        listaDiscos.remove(posicion);
    }
    public void guardarCancion(Cancion cancion){listaCanciones.add(cancion);
    }
    public void eliminarCancion(int posicion){
        listaCanciones.remove(posicion);
    }

    public ArrayList<Grupo> getListaGrupos() {
        return listaGrupos;
    }

    public void setListaGrupos(ArrayList<Grupo> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }

    public ArrayList<Disco> getListaDiscos() {
        return listaDiscos;
    }

    public void setListaDiscos(ArrayList<Disco> listaDiscos) {
        this.listaDiscos = listaDiscos;
    }

    public ArrayList<Cancion> getListaCanciones() {
        return listaCanciones;
    }

    public void setListaCanciones(ArrayList<Cancion> listaCanciones) {
        this.listaCanciones = listaCanciones;
    }


   public void exportarXML(){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        org.w3c.dom.Document documento = null;
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null ,"xml",null);

            org.w3c.dom.Element raiz =documento.createElement("Discotek");
            documento.getDocumentElement().appendChild(raiz);

            org.w3c.dom.Element nodoGrupo = null;
            org.w3c.dom.Element nodoDatosGrupo = null;
            org.w3c.dom.Element nodoDisco = null;
            org.w3c.dom.Element nodoDatosDisco = null;
            org.w3c.dom.Element nodoCancion = null;
            org.w3c.dom.Element nodoDatosCancion = null;
            Text texto = null;

            for(Grupo grupo : listaGrupos){
                nodoGrupo = documento.createElement("Grupo");
                raiz.appendChild(nodoGrupo);

                nodoDatosGrupo =  documento.createElement("Nombre_Grupo");
                nodoGrupo.appendChild(nodoDatosGrupo);
                texto = documento.createTextNode(grupo.getNombre());
                nodoDatosGrupo.appendChild(texto);

                nodoDatosGrupo =  documento.createElement("Genero_Grupo");
                nodoGrupo.appendChild(nodoDatosGrupo);
                texto = documento.createTextNode(grupo.getGenero());
                nodoDatosGrupo.appendChild(texto);

                nodoDatosGrupo = documento.createElement("Fecha_Inicio_Grupo");
                nodoGrupo.appendChild(nodoDatosGrupo);
                texto = documento.createTextNode(String.valueOf(grupo.getFechainicio()));
                nodoDatosGrupo.appendChild(texto);

                nodoDatosGrupo = documento.createElement("Fecha_Fin_Grupo");
                nodoGrupo.appendChild(nodoDatosGrupo);
                texto = documento.createTextNode(String.valueOf(grupo.getFechafin()));
                nodoDatosGrupo.appendChild(texto);

                nodoDatosGrupo =  documento.createElement("Nacionalidad_Grupo");
                nodoGrupo.appendChild(nodoDatosGrupo);
                texto = documento.createTextNode(grupo.getNacionalidad());
                nodoDatosGrupo.appendChild(texto);


            }

            for(Disco disco : listaDiscos){
                nodoDisco = documento.createElement("Disco");
                raiz.appendChild(nodoDisco);

                nodoDatosDisco = documento.createElement("Titulo_Disco");
                 nodoDisco.appendChild(nodoDatosDisco);
                texto = documento.createTextNode(disco.getTitulo());
                 nodoDatosDisco.appendChild(texto);

                nodoDatosDisco =  documento.createElement("Duracion_Disco");
                nodoDisco.appendChild(nodoDatosDisco);
                texto = documento.createTextNode(String.valueOf(disco.getDuracion()));
                 nodoDatosDisco.appendChild(texto);

                nodoDatosDisco =  documento.createElement("Productora_Disco");
                nodoDisco.appendChild(nodoDatosDisco);
                texto = documento.createTextNode(disco.getProductora());
                nodoDatosDisco.appendChild(texto);

                nodoDatosDisco =  documento.createElement("N_Canciones_Disco");
                 nodoDisco.appendChild(nodoDatosDisco);
                texto = documento.createTextNode(String.valueOf(disco.getNumcanciones()));
                nodoDatosDisco.appendChild(texto);

                nodoDatosDisco =  documento.createElement("Precio_Disco");
                nodoDisco.appendChild(nodoDatosDisco);
                texto = documento.createTextNode(String.valueOf(disco.getPrecio()));
                nodoDatosDisco.appendChild(texto);

                nodoDatosDisco = documento.createElement("Disco_NombreGrupo");
                nodoDisco.appendChild(nodoDatosDisco);
                texto = documento.createTextNode(String.valueOf(disco.getNombreGrupo()));
                nodoDatosDisco.appendChild(texto);

            }

            for(Cancion cancion : listaCanciones){
                nodoCancion =  documento.createElement("Cancion");
                raiz.appendChild(nodoCancion);

                nodoDatosCancion =  documento.createElement("Titulo_Cancion");
                nodoCancion.appendChild(nodoDatosCancion);
                texto = documento.createTextNode(cancion.getTitulo());
                nodoDatosCancion.appendChild(texto);

                nodoDatosCancion =  documento.createElement("Duracion_Cancion");
                nodoCancion.appendChild(nodoDatosCancion);
                texto = documento.createTextNode(cancion.getTitulo());
                nodoDatosCancion.appendChild(texto);

                nodoDatosCancion = documento.createElement("Track_Cancion");
                nodoCancion.appendChild(nodoDatosCancion);
                texto = documento.createTextNode(cancion.getTitulo());
                nodoDatosCancion.appendChild(texto);

                nodoDatosCancion =  documento.createElement("Precio_Cancion");
                nodoCancion.appendChild(nodoDatosCancion);
                texto = documento.createTextNode(cancion.getTitulo());
                nodoDatosCancion.appendChild(texto);

                nodoDatosCancion =  documento.createElement("Genero_Cancion");
                nodoCancion.appendChild(nodoDatosCancion);
                texto = documento.createTextNode(cancion.getTitulo());
                nodoDatosCancion.appendChild(texto);

                nodoDatosCancion =  documento.createElement("Nombre_Grupo");
                nodoCancion.appendChild(nodoDatosCancion);
                texto = documento.createTextNode(cancion.getTitulo());
                nodoDatosCancion.appendChild(texto);

                nodoDatosCancion =  documento.createElement("Nombre_Disco");
                nodoCancion.appendChild(nodoDatosCancion);
                texto = documento.createTextNode(cancion.getTitulo());
                nodoDatosCancion.appendChild(texto);
            }
            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(new File(Constantes.PATHXML));

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source,resultado);

        }catch(ParserConfigurationException pce){
            pce.printStackTrace();
        }catch(TransformerConfigurationException tce){
            tce.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

   }


   /* public void importarXML()  {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        org.w3c.dom.Document documento = null;



        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            documento =  builder.parse(new File(Constantes.PATHXML));

            NodeList Grupo = documento.getElementsByTagName("Discotek");
            NodeList Disco = documento.getElementsByTagName("Discotek");
            NodeList Cancion = documento.getElementsByTagName("Discotek");

            for(int i=0;i<Grupo.getLength();i++){
                Node Discotek = Grupo.item(i);
                Discotek.get
                Element elemento = (Element) Discotek;

                System.out.println(elemento.);
            }

            for(int i=0;i<Disco.getLength();i++){

            }

            for(int i=0;i< Cancion.getLength();i++){

            }

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch(ParserConfigurationException pce){
            pce.printStackTrace();
        }

    }*/
}
