package com.dvd.msk.gui;

import javax.swing.*;

/**
 * Created by k3ym4n on 03/12/2015.
 */
public class Botonera {
    public JPanel panelbotonera;
    public JButton btnuevo;
    public JButton btguardar;
    public JButton btmodificar;
    public JButton btborrar;
    public JButton btbuscar;
    public JButton btguardarcomo;
    public JButton btexpXML;
    public JButton btimpXML;
    public JButton btcancelar;
}
