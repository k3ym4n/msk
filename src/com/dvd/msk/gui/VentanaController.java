package com.dvd.msk.gui;

import com.dvd.msk.Botonera;
import com.dvd.msk.Ventana;
import com.dvd.msk.base.Cancion;
import com.dvd.msk.base.Disco;
import com.dvd.msk.base.Grupo;
import com.dvd.msk.util.Util;


import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.String;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by k3ym4n on 03/12/2015.
 */
public class VentanaController implements ActionListener,KeyListener,ListSelectionListener {

    private VentanaModel model;
    private Ventana view;
    private Botonera botones;

    private int posicionGrupo;
    private int posicionDisco;
    private int posicionCancion;

    private DefaultListModel<Grupo> modeloListaGrupo;
    private DefaultListModel<Disco> modeloListaDisco;
    private DefaultListModel<Cancion> modeloListaCancion;

    private ArrayList<Grupo> listaGrupos;
    private ArrayList<Disco> listaDiscos;


    private enum Pestana {
        GRUPO, DISCO, CANCION;
    }

    private boolean nuevoGrupo;
    private boolean nuevoDisco;
    private boolean nuevoCancion;

    private boolean modificar;


    public VentanaController(Ventana view, VentanaModel model, Botonera botones) {
        this.view = view;
        this.model = model;
        this.botones = botones;

        //pegar modeloLista... para verlo en el Jlista....   ** ... lo que sea grupos , cancion, disco
        modeloListas(Pestana.GRUPO);
        modeloListas(Pestana.DISCO);
        modeloListas(Pestana.CANCION);
        //poner Listener a los botones , que en esta clase es un objeto.
        botones.btnuevo.addActionListener(this);
        botones.btguardar.addActionListener(this);
        botones.btmodificar.addActionListener(this);
        botones.btborrar.addActionListener(this);
//        botones.btbuscar.addActionListener(this);
        botones.btcancelar.addActionListener(this);
        botones.btguardarcomo.addActionListener(this);
        botones.btexpXML.addActionListener(this);
        botones.btimpXML.addActionListener(this);
        //poner KeyListener a las cajas de busqueda
        view.tfgrupobuscar.addKeyListener(this);
        view.tfdiscobuscar.addKeyListener(this);
        view.tfcancionbuscar.addKeyListener(this);


        posicionGrupo = 0;
        posicionDisco = 0;
        posicionCancion = 0;


        //los escuchadres de las pesta�as
        view.Panelprincipal.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int pestana = view.Panelprincipal.getSelectedIndex();
                switch (pestana) {
                    case 0:
                        System.out.println("pestana grupo");
                        edicion(Pestana.GRUPO, false);
                        posicionGrupo = 0;
                        cargarPestanas(Pestana.GRUPO);
                        break;
                    case 1:
                        System.out.println("pestana Disco");
                        edicion(Pestana.DISCO, false);
                        posicionDisco = 0;
                        cargarPestanas(Pestana.DISCO);
                        break;
                    case 2:
                        System.out.println("pestana cancion");
                        edicion(Pestana.CANCION, false);
                        posicionCancion = 0;
                        cargarPestanas(Pestana.CANCION);
                        break;
                }
            }
        });

        listenerJL(this, Pestana.GRUPO);
        listenerJL(this, Pestana.DISCO);
        listenerJL(this, Pestana.CANCION);

        cargarPestanas(Pestana.GRUPO);
        cargarPestanas(Pestana.DISCO);
        cargarPestanas(Pestana.CANCION);

        refrescarLista(Pestana.GRUPO);
        refrescarLista(Pestana.DISCO);
        refrescarLista(Pestana.CANCION);

        limpiarcajas(Pestana.GRUPO);
        limpiarcajas(Pestana.DISCO);
        limpiarcajas(Pestana.CANCION);
    }


    //este metodo escucha las listas
    @Override
    public void valueChanged(ListSelectionEvent e) {
        int pestana = view.Panelprincipal.getSelectedIndex();
        switch (pestana) {
            case 0:
                view.JlistaGrupos.addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        posicionGrupo = view.JlistaGrupos.getSelectedIndex();
                        if (posicionGrupo == -1) {
                            return;
                        }
                        cargarPestanas(Pestana.GRUPO);
                        botones.btmodificar.setEnabled(true);
                        botones.btborrar.setEnabled(true);
                    }
                });
                break;
            case 1:

                view.JlistaDiscos.addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        posicionDisco = view.JlistaDiscos.getSelectedIndex();
                        if (posicionDisco == -1) {
                            return;
                        }
                        cargarComboboxDisco();
                        cargarPestanas(Pestana.DISCO);
                        botones.btmodificar.setEnabled(true);
                        botones.btborrar.setEnabled(true);
                    }
                });
                break;
            case 2:
                view.JlistaCanciones.addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        posicionCancion = view.JlistaCanciones.getSelectedIndex();
                        if (posicionCancion == -1) {
                            return;
                        }
                        cargarComboboxCancionGrupo();
                        cargarComboboxCancionDisco();
                        cargarPestanas(Pestana.CANCION);
                        botones.btmodificar.setEnabled(true);
                        botones.btcancelar.setEnabled(true);
                    }
                });
                break;
            default:
                break;
        }
    }

    //Escucha los botones y hace acciones en consecuencia
    @Override
    public void actionPerformed(ActionEvent e) {
        int indice = view.Panelprincipal.getSelectedIndex();

        if (e.getSource() == botones.btnuevo) {
            switch (indice) {
                case 0:
                    System.out.println("boton nuevo grupo");
                    edicion(Pestana.GRUPO, true);
                    modificar = false;
                    limpiarcajas(Pestana.GRUPO);
                    break;
                case 1:
                    System.out.println("boton nuevo discos");
                    edicion(Pestana.DISCO, true);
                    modificar = false;
                    cargarComboboxDisco();
                    limpiarcajas(Pestana.DISCO);
                    break;
                case 2:
                    System.out.println("boton nuevo cancion");
                    edicion(Pestana.CANCION, true);
                    modificar = false;
                    cargarComboboxCancionDisco();
                    cargarComboboxCancionGrupo();
                    limpiarcajas(Pestana.CANCION);
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botones.btguardar) {
            switch (indice) {
                case 0:
                    Grupo grupo = null;
                    if (modificar) {
                        grupo = model.getListaGrupos().get(view.JlistaGrupos.getSelectedIndex());
                    } else {
                        grupo = new Grupo();
                        nuevoGrupo = true;
                        System.out.println("guardar objeto grupo");
                    }

                    if(view.tfgruponombre.getText().equals("")){
                        Util.campoObligatorio("El nombre del grupo es un campo obligatorio" , "Nombre del grupo");
                        return;
                    }
                    grupo.setNombre(view.tfgruponombre.getText());
                    grupo.setGenero(view.tfgrupogenero.getText());
                    grupo.setFechainicio(view.dategrupofinicio.getDate());
                    //todo controlar error de fecha de inicio mayor que fecha fin
                    //todo campo obligatorio fecha inicio showdialog
                    grupo.setFechafin(view.dategrupoffin.getDate());
                    if (view.dategrupofinicio.getDate().after(view.dategrupoffin.getDate())) {
                        Util.coherenciadefechas("Fecha de fin es menor quer la fecha de inicio", "Imposible");

                    } else {
                        grupo.setNacionalidad(view.tfgruponacionalidad.getText());
                    }


                    if (nuevoGrupo) {
                        model.guardarGrupo(grupo);
                    }
                    refrescarLista(Pestana.GRUPO);
                    model.guardarFicheroGrupo();
                    limpiarcajas(Pestana.GRUPO);
                    System.out.println("grupo guardado");
                    break;
                case 1:
                    Disco disco = null;
                    if (modificar) {
                        disco = model.getListaDiscos().get(view.JlistaDiscos.getSelectedIndex());
                    } else {
                        disco = new Disco();
                        nuevoDisco = true;
                        System.out.println("guardar objeto disco");
                    }
                    disco.setTitulo(view.tfdiscotitulo.getText());
                    //todo el valor numerico debe ser correcto si no se a�adira un punto 0 (.0) al final de manera automatica
                   /* if(duracionesUnnumero(view.floatdiscoduracion.getText())== false  ){
                        Util.campoObligatorio("El valor de duracion debe ser un numero" , "DUARACION");
                        return;
                    }else{
                        disco.setDuracion(Float.parseFloat(view.floatdiscoduracion.getText()));
                    }*/

                    if(Util.esNumero(view.floatdiscoduracion.getText())== false  ){
                        Util.campoObligatorio("El valor de duracion debe ser un numero" , "DURACION");
                        return;
                    }else{
                        disco.setDuracion(Float.parseFloat(view.floatdiscoduracion.getText()));
                    }

                    disco.setProductora(view.tfdiscoproductora.getText());
                    //TODO asegurarse que sea un numero
                    if(Util.esNumero(view.intdisconumcanciones.getText())== false){
                        Util.campoObligatorio("El valor de numero de canciones debe ser numerico" , "Numero de canciones");
                        return;
                    }else{
                        disco.setNumcanciones(Integer.parseInt(view.intdisconumcanciones.getText()));
                    }

                    if(Util.esNumero(view.floatdiscoprecio.getText())== false){
                        Util.campoObligatorio("El precio del discp debe ser un numero" , "Precio disco");
                        return;
                    }else{
                        disco.setPrecio(Float.parseFloat(view.floatdiscoprecio.getText()));
                    }

                    //todo campo obligatorio el disco pertenece a un grupo
                    disco.setNombreGrupo(String.valueOf(view.combodiscogrupo.getSelectedItem()));

                    if (nuevoDisco) {
                        model.guardarDisco(disco);
                    }
                    refrescarLista(Pestana.DISCO);
                    model.guardarFicheroDisco();
                    limpiarcajas(Pestana.DISCO);
                    System.out.println("disco guardado");
                    break;
                case 2:

                    Cancion cancion = null;
                    if (modificar) {
                        cancion = model.getListaCanciones().get(view.JlistaCanciones.getSelectedIndex());
                    } else {
                        cancion = new Cancion();
                        nuevoCancion = true;
                    }
                        if(view.tfcanciontitulo.getText().equals("")){
                            Util.campoObligatorio("El titulo es un cxampo obligatorio" , "TITULO");
                            return;
                        }else{
                            cancion.setTitulo(view.tfcanciontitulo.getText());
                        }

                    //todo el valor numerico debe ser correcto
                        if(Util.esNumero(view.floatcancionduracion.getText())== false){
                            Util.campoObligatorio("La duracion de la cancion debe ser un campo numerico" , "Duracion Cancion");
                            return;
                        }else{
                            cancion.setDuracion(Float.parseFloat(view.floatcancionduracion.getText()));
                        }

                        if(Util.esNumero(view.intcanciontrack.getText())== false){
                            Util.campoObligatorio("El precio es un campo numerico" , "PRECIO CANCION");
                            return;
                        }else{
                            cancion.setTrack(Integer.parseInt(view.intcanciontrack.getText()));
                        }

                    //todo el valor numerico debe ser correcto
                    if(Util.esNumero(view.floatcancionprecio.getText()) == false){
                        Util.campoObligatorio("La duracion es un campo numerico" , "DUARACION CANCION");
                    }else{
                        cancion.setPrecio(Float.parseFloat(view.floatcancionprecio.getText()));
                    }

                    cancion.setGenero(view.tfcanciongenero.getText());

                    //todo campo obligatorio
                    cancion.setNombreGrupo(String.valueOf(view.combocanciongrupo.getSelectedItem()));
                    cancion.setNombreDisco(String.valueOf(view.combocanciondisco.getSelectedItem()));


                    if (nuevoCancion) {
                        model.guardarCancion(cancion);
                    }
                    refrescarLista(Pestana.CANCION);
                    model.guardarFicheroCancion();
                    limpiarcajas(Pestana.CANCION);
                    System.out.println("cancion guardada");
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botones.btmodificar) {
            switch (indice) {
                case 0:
                    botones.btnuevo.setEnabled(true);
                    modificar = true;
                    nuevoGrupo = false;
                    view.JlistaGrupos.addListSelectionListener(new ListSelectionListener() {
                        @Override
                        public void valueChanged(ListSelectionEvent e) {
                            posicionGrupo = view.JlistaGrupos.getSelectedIndex();
                            if (posicionGrupo == -1) {
                                return;
                            }
                            cargarPestanas(Pestana.GRUPO);
                            botones.btmodificar.setEnabled(true);
                            botones.btborrar.setEnabled(true);
                            botones.btnuevo.setEnabled(true);
                        }
                    });
                    break;
                case 1:
                    botones.btnuevo.setEnabled(true);
                    modificar = true;
                    nuevoDisco = false;
                    view.JlistaDiscos.addListSelectionListener(new ListSelectionListener() {
                        @Override
                        public void valueChanged(ListSelectionEvent e) {
                            posicionDisco = view.JlistaDiscos.getSelectedIndex();
                            if (posicionDisco == -1) {
                                return;
                            }
                            cargarPestanas(Pestana.DISCO);
                            botones.btmodificar.setEnabled(true);
                            botones.btborrar.setEnabled(true);
                            botones.btnuevo.setEnabled(true);
                        }
                    });
                    break;
                case 2:
                    botones.btnuevo.setEnabled(true);
                    modificar = true;
                    nuevoCancion = false;
                    view.JlistaCanciones.addListSelectionListener(new ListSelectionListener() {
                        @Override
                        public void valueChanged(ListSelectionEvent e) {
                            posicionCancion = view.JlistaCanciones.getSelectedIndex();
                            if (posicionCancion == -1) {
                                return;
                            }
                            cargarPestanas(Pestana.CANCION);
                            botones.btmodificar.setEnabled(true);
                            botones.btborrar.setEnabled(true);
                            botones.btnuevo.setEnabled(true);
                        }
                    });
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botones.btborrar) {
            cargarComboboxCancionGrupo();
            cargarComboboxCancionDisco();
            cargarComboboxDisco();
            switch (indice) {

                case 0:
                    if ((Util.borrarConfirmacion("�SEGURO?")) == JOptionPane.YES_OPTION) {
                        posicionGrupo = view.JlistaGrupos.getSelectedIndex();
                        model.eliminarGrupo(posicionGrupo);
                        model.guardarFicheroGrupo();
                        refrescarLista(Pestana.GRUPO);
                        limpiarcajas(Pestana.GRUPO);

                    } else {
                        refrescarLista(Pestana.GRUPO);
                        limpiarcajas(Pestana.GRUPO);
                    }
                    break;
                case 1:
                    if ((Util.borrarConfirmacion("�SEGURO?")) == JOptionPane.YES_OPTION) {
                        posicionDisco = view.JlistaDiscos.getSelectedIndex();
                        model.eliminarDisco(posicionDisco);
                        model.guardarFicheroDisco();
                        refrescarLista(Pestana.DISCO);
                        limpiarcajas(Pestana.DISCO);
                    } else {
                        refrescarLista(Pestana.DISCO);
                        limpiarcajas(Pestana.DISCO);
                    }


                    break;
                case 2:
                    if ((Util.borrarConfirmacion("�SEGURO?")) == JOptionPane.YES_OPTION) {
                        posicionCancion = view.JlistaCanciones.getSelectedIndex();
                        model.eliminarCancion(posicionCancion);
                        model.guardarFicheroCancion();
                        refrescarLista(Pestana.CANCION);
                        limpiarcajas(Pestana.CANCION);
                    } else {
                        refrescarLista(Pestana.CANCION);
                        limpiarcajas(Pestana.CANCION);
                    }
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botones.btcancelar) {
            switch (indice) {
                case 0:
                    limpiarcajas(Pestana.GRUPO);
                    break;
                case 1:
                    limpiarcajas(Pestana.DISCO);
                    break;
                case 2:
                    limpiarcajas(Pestana.CANCION);
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botones.btexpXML) {
            switch (indice) {
                case 0:
                    model.exportarXML();
                    break;
                case 1:
                    model.exportarXML();
                    break;
                case 2:
                    model.exportarXML();
                    break;
            }
        }
    }

    private void cargarPestanas(Pestana pestana) {
        switch (pestana) {
            case GRUPO:
                if (model.listaGrupos.size() == 0) {
                    return;
                }
                Grupo grupo = model.getListaGrupos().get(posicionGrupo);

                view.tfgruponombre.setText(grupo.getNombre());
                view.tfgrupogenero.setText(grupo.getGenero());
                view.dategrupofinicio.setDate(grupo.getFechainicio());
                view.dategrupoffin.setDate(grupo.getFechafin());
                view.tfgruponacionalidad.setText(grupo.getNacionalidad());
                //TODO �Como se pasan las canciones a  listagrupocanciones?

                break;
            case DISCO:
                if (model.listaDiscos.size() == 0) {
                    return;
                }
                Disco disco = model.getListaDiscos().get(posicionDisco);

                view.tfdiscotitulo.setText(disco.getTitulo());
                view.floatdiscoduracion.setText(String.valueOf(disco.getDuracion()));
                view.tfdiscoproductora.setText(disco.getProductora());
                view.intdisconumcanciones.setText(String.valueOf(disco.getNumcanciones()));
                view.floatdiscoprecio.setText(String.valueOf(disco.getPrecio()));

                break;
            case CANCION:

                if (model.listaCanciones.size() == 0) {
                    return;
                }
                Cancion cancion = model.getListaCanciones().get(posicionCancion);

                view.tfcanciontitulo.setText(cancion.getTitulo());
                view.floatcancionduracion.setText(String.valueOf(cancion.getDuracion()));
                view.intcanciontrack.setText(String.valueOf(cancion.getTrack()));
                view.floatcancionprecio.setText(String.valueOf(cancion.getPrecio()));
                view.tfcanciongenero.setText(String.valueOf(cancion.getGenero()));


                break;
            default:
                break;
        }
    }

    private void limpiarcajas(Pestana pestana) {
        switch (pestana) {
            case GRUPO:
                view.tfgruponombre.setText("");
                view.tfgrupogenero.setText("");
                view.dategrupofinicio.setDate(null);
                view.dategrupoffin.setDate(null);
                view.tfgruponacionalidad.setText("");
                break;
            case DISCO:
                view.tfdiscotitulo.setText("");
                view.floatdiscoduracion.setText("");
                view.tfdiscoproductora.setText("");
                view.intdisconumcanciones.setText("");
                view.floatdiscoprecio.setText("");
                view.combodiscogrupo.setSelectedItem(null);
                break;
            case CANCION:
                view.tfcanciontitulo.setText("");
                view.floatcancionduracion.setText("");
                view.intcanciontrack.setText("");
                view.floatcancionprecio.setText("");
                view.tfcanciongenero.setText("");
                view.combocanciongrupo.setSelectedItem(null);
                view.combocanciondisco.setSelectedItem(null);
                break;
            default:
                break;
        }
    }

    private void edicion(Pestana pestana, boolean D) {
        switch (pestana) {
            case GRUPO:
                if (D) {
                    view.tfgruponombre.setText("");
                    view.tfgrupogenero.setText("");
                    view.dategrupofinicio.setEnabled(true);
                    view.dategrupoffin.setEnabled(true);
                    view.tfgruponacionalidad.setText("");
                    view.tfgrupobuscar.setText("BUSCAR");
                }
                view.tfgruponombre.setEditable(D);
                view.tfgrupogenero.setEditable(D);
                view.dategrupofinicio.setEnabled(D);
                view.dategrupoffin.setEnabled(D);
                view.tfgruponacionalidad.setEditable(D);
                view.tfgrupobuscar.setEnabled(D);

                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case DISCO:
                if (D) {
                    view.tfdiscotitulo.setText("");
                    view.floatcancionduracion.setText("");
                    view.tfdiscoproductora.setText("");
                    view.intdisconumcanciones.setText("");
                    view.floatdiscoprecio.setText("");
                    view.combodiscogrupo.setSelectedItem(null);
                    view.tfdiscobuscar.setText("BUSCAR");
                }
                view.tfdiscotitulo.setEnabled(D);
                view.floatdiscoduracion.setEnabled(D);
                view.tfdiscoproductora.setEnabled(D);
                view.intdisconumcanciones.setEnabled(D);
                view.floatdiscoprecio.setEnabled(D);
                view.combodiscogrupo.setEnabled(D);

                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case CANCION:
                if (D) {
                    view.tfcanciontitulo.setText("");
                    view.floatcancionduracion.setText("");
                    view.intcanciontrack.setText("");
                    view.floatcancionprecio.setText("");
                    view.tfcanciongenero.setText("");
                    view.combocanciongrupo.setSelectedItem(null);
                    view.combocanciondisco.setSelectedItem(null);
                    view.tfcancionbuscar.setText("BUSCAR");
                }
                view.tfcanciontitulo.setEnabled(D);
                view.floatcancionduracion.setEnabled(D);
                view.intcanciontrack.setEnabled(D);
                view.floatcancionprecio.setEnabled(D);
                view.tfcanciongenero.setEnabled(D);

                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            default:
                break;
        }
    }


    /* //a�adir texto a los combobox pertinentes
        view.combodiscogrupo.addItem("Grupo");
        view.combocanciongrupo.addItem("grupo");
        view.combocanciondisco.addItem("disco");*/
    private void cargarComboboxDisco() {
        System.out.println("Entra en el combo");
        view.combodiscogrupo.removeAllItems();
        for (Grupo grupo : model.getListaGrupos()) {
            view.combodiscogrupo.addItem(grupo.getNombre());
        }
        System.out.println("sale del combo");
    }

    private void cargarComboboxCancionGrupo() {
        view.combocanciongrupo.removeAllItems();
        for (Grupo grupo : model.getListaGrupos()) {
            view.combocanciongrupo.addItem(grupo.getNombre());
        }
    }

    private void cargarComboboxCancionDisco() {
        view.combocanciondisco.removeAllItems();
        for (Disco disco : model.getListaDiscos()) {
            view.combocanciondisco.addItem(disco.getTitulo());
        }
    }

    private void refrescarLista(Pestana pestana) {
        switch (pestana) {
            case GRUPO:
                modeloListaGrupo.removeAllElements();
                for (Grupo grupo : model.getListaGrupos()) {
                    modeloListaGrupo.addElement(grupo);
                }
                break;
            case DISCO:
                modeloListaDisco.removeAllElements();
                for (Disco disco : model.getListaDiscos()) {
                    modeloListaDisco.addElement(disco);
                }
                break;
            case CANCION:
                modeloListaCancion.removeAllElements();
                for (Cancion cancion : model.getListaCanciones()) {
                    modeloListaCancion.addElement(cancion);
                }
                break;
            default:
                break;
        }
    }

    private void listenerJL(ListSelectionListener listener, Pestana pestana) {
        switch (pestana) {
            case GRUPO:
                view.JlistaGrupos.addListSelectionListener(listener);
                break;
            case DISCO:
                view.JlistaDiscos.addListSelectionListener(listener);
                break;
            case CANCION:
                view.JlistaCanciones.addListSelectionListener(listener);
                break;
        }
    }

    private void modeloListas(Pestana pestana) {
        switch (pestana) {
            case GRUPO:
                modeloListaGrupo = new DefaultListModel<>();
                view.JlistaGrupos.setModel(modeloListaGrupo);
                break;
            case DISCO:
                modeloListaDisco = new DefaultListModel<>();
                view.JlistaDiscos.setModel(modeloListaDisco);
                break;
            case CANCION:
                modeloListaCancion = new DefaultListModel<>();
                view.JlistaCanciones.setModel(modeloListaCancion);
                break;
        }

    }

    private void eliminarConfirmacion() {
        if ((Util.borrarConfirmacion("�SEGURO?")) == JOptionPane.YES_OPTION) {
            return;
        }
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        //TODO el campo se seleccione el buscar desaparece y cuando este vacio aparece toda la lista
        int indice = view.Panelprincipal.getSelectedIndex();
        //creo un vector y los muestro en el sout
        Vector<Grupo> encuentraCrupo = new Vector<>();
        Vector<Disco> encuentraDisco = new Vector<>();
        Vector<Cancion> encuentraCancion = new Vector<>();
        switch (indice) {
            case 0:

                String grupob = view.tfgrupobuscar.getText();
                System.out.println(grupob);
                for (int i = 0; i < model.listaGrupos.size(); i++) {
                    if (model.listaGrupos.get(i).getNombre().startsWith(grupob)) {
                        encuentraCrupo.add(model.listaGrupos.get(i));
                    }
                }

                modeloListaGrupo.removeAllElements();
                for (Grupo grupo : encuentraCrupo) {
                    modeloListaGrupo.addElement(grupo);
                }

                if (grupob.equals("")) {
                    refrescarLista(Pestana.GRUPO);
                }
                break;
            case 1:
                String discob = view.tfdiscobuscar.getText();
                System.out.println(discob);
                for (int i = 0; i < modeloListaDisco.size(); i++) {
                    if (model.listaDiscos.get(i).getTitulo().startsWith(discob)) {
                        encuentraDisco.add(model.listaDiscos.get(i));
                    }
                }

                modeloListaDisco.removeAllElements();
                for (Disco disco : encuentraDisco) {
                    modeloListaDisco.addElement(disco);
                }
                if (discob.equals("")) {
                    refrescarLista(Pestana.DISCO);
                }
                break;
            case 2:
                String cancionb = view.tfcancionbuscar.getText();
                System.out.println(cancionb);
                for (int i = 0; i < model.listaCanciones.size(); i++) {
                    if (model.listaCanciones.get(i).getTitulo().startsWith(cancionb)) {
                        encuentraCancion.add(model.listaCanciones.get(i));
                    }
                }

                modeloListaCancion.removeAllElements();
                for (Cancion cancion : encuentraCancion) {
                    modeloListaCancion.addElement(cancion);
                }

                if (cancionb.equals("")) {
                    refrescarLista(Pestana.CANCION);
                }

                break;
        }
    }

    private boolean duracionesUnnumero(String duracion) {

        duracion = view.floatdiscoduracion.getText();

        if (duracion == null  || duracion.isEmpty()) {
            return false;
        }
        int i = 0;
        if (duracion.charAt(0) == '-' ) {
            if (duracion.length() > 1 ) {
                i++;
            } else {
                return false;
            }
        }
        for (; i < duracion.length(); i++) {
            if (!Character.isDigit(duracion.charAt(i))) {
                return false;
            }
        }

        return true;

    }


    }

//duracion,numero,precio
   /* private boolean duracionesUnnumero(String duracion) {

                duracion = view.floatdiscoduracion.getText();

                if (duracion == null  || duracion.isEmpty()) {
                    return false;
                }
                int i = 0;
                if (duracion.charAt(0) == '-' ) {
                    if (duracion.length() > 1 ) {
                        i++;
                    } else {
                        return false;
                    }
                }
                for (; i < duracion.length(); i++) {
                    if (!Character.isDigit(duracion.charAt(i))) {
                        return false;
                    }
                }

                return true;

        }*/

//Edicion de botones
 /* private void campos(Pestana pestana,boolean D){
        switch(pestana){
            case GRUPO:
                if(D){
                    view.tfgruponombre.setText("");
                    view.tfgrupogenero.setText("");
                    view.dategrupofinicio.setEnabled(true);
                    view.dategrupoffin.setEnabled(true);
                    view.tfgruponacionalidad.setText("");
                    view.tfgrupobuscar.setText("BUSCAR");
                }
                break;
            case DISCO:
                if(D){
                    view.tfdiscotitulo.setText("");
                    view.floatcancionduracion.setText("");
                    view.tfdiscoproductora.setText("");
                    view.intdisconumcanciones.setText("");
                    view.floatdiscoprecio.setText("");
                    view.combodiscogrupo.setSelectedItem(null);
                    view.tfdiscobuscar.setText("BUSCAR");
                }
                break;
            case CANCION:
                if(D){
                    view.tfcanciontitulo.setText("");
                    view.floatcancionduracion.setText("");
                    view.intcanciontrack.setText("");
                    view.floatcancionprecio.setText("");
                    view.tfcanciongenero.setText("");
                    view.combocanciongrupo.setSelectedItem(null);
                    view.combocanciondisco.setSelectedItem(null);
                    view.tfcancionbuscar.setText("BUSCAR");
                }
                break;
        }
    }
    private void edicionbotonesInicio(Pestana pestana,boolean D){
        switch(pestana){
            case GRUPO:
                if(D){
                    campos(Pestana.DISCO,true);
                    botones.btnuevo.setEnabled(D);
                    botones.btcancelar.setEnabled(!D);
                    botones.btguardar.setEnabled(!D);
                    botones.btmodificar.setEnabled(!D);
                    botones.btguardarcomo.setEnabled(D);
                    botones.btexpXML.setEnabled(D);
                    botones.btimpXML.setEnabled(D);
                }

                break;
            case DISCO:
                if(D){
                    campos(Pestana.DISCO,true);
                    botones.btnuevo.setEnabled(D);
                    botones.btcancelar.setEnabled(!D);
                    botones.btguardar.setEnabled(!D);
                    botones.btmodificar.setEnabled(!D);
                    botones.btguardarcomo.setEnabled(D);
                    botones.btexpXML.setEnabled(D);
                    botones.btimpXML.setEnabled(D);
                }

                break;
            case CANCION:
                if(D){
                    campos(Pestana.CANCION,true);
                    botones.btnuevo.setEnabled(D);
                    botones.btcancelar.setEnabled(!D);
                    botones.btguardar.setEnabled(!D);
                    botones.btmodificar.setEnabled(!D);
                    botones.btguardarcomo.setEnabled(D);
                    botones.btexpXML.setEnabled(D);
                    botones.btimpXML.setEnabled(D);
                }

                break;
        }
    }

    private void edicionNuevo(Pestana pestana, boolean D){
        switch(pestana){
            case GRUPO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(!D);
                botones.btimpXML.setEnabled(!D);
                break;
            case DISCO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(!D);
                botones.btimpXML.setEnabled(!D);
                break;
            case CANCION:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(!D);
                botones.btimpXML.setEnabled(!D);
                break;
        }
    }
    private void edicionGuardar(Pestana pestana,boolean D){
        switch(pestana){
            case GRUPO:
                botones.btnuevo.setEnabled(D);
                botones.btcancelar.setEnabled(!D);
                botones.btguardar.setEnabled(!D);
                botones.btmodificar.setEnabled(D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case DISCO:
                botones.btnuevo.setEnabled(D);
                botones.btcancelar.setEnabled(!D);
                botones.btguardar.setEnabled(!D);
                botones.btmodificar.setEnabled(D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case CANCION:
                botones.btnuevo.setEnabled(D);
                botones.btcancelar.setEnabled(!D);
                botones.btguardar.setEnabled(!D);
                botones.btmodificar.setEnabled(D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
        }
    }
    private void edicionModificar(Pestana pestana,boolean D){
        switch(pestana){
            case GRUPO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case DISCO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case CANCION:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
        }
    }
    private void edicionBorrar(Pestana pestana,boolean D){
        switch(pestana){
            case GRUPO:
                botones.btnuevo.setEnabled(D);
                botones.btcancelar.setEnabled(!D);
                botones.btguardar.setEnabled(!D);
                botones.btmodificar.setEnabled(D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case DISCO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case CANCION:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
        }
    }
    private void edicionCancelar(Pestana pestana,boolean D){
        switch(pestana){
            case GRUPO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case DISCO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case CANCION:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
        }
    }

   private void edicionGuardarComo(Pestana pestana,boolean D){
        switch(pestana){
            case GRUPO:
                botones.btnuevo.setEnabled(D);
                botones.btcancelar.setEnabled(!D);
                botones.btguardar.setEnabled(!D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btexpXML.setEnabled(!D);
                botones.btimpXML.setEnabled(!D);
                break;
            case DISCO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case CANCION:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
        }
    }
    private void edicionExpXML(Pestana pestana , boolean D){
        switch(pestana){
            case GRUPO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case DISCO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case CANCION:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
        }
    }
    private void edicionImpXML(Pestana pestana , boolean D){
        switch(pestana){
            case GRUPO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case DISCO:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
            case CANCION:
                botones.btnuevo.setEnabled(!D);
                botones.btcancelar.setEnabled(D);
                botones.btguardar.setEnabled(D);
                botones.btmodificar.setEnabled(!D);
                botones.btguardarcomo.setEnabled(D);
                botones.btguardarcomo.setEnabled(!D);
                botones.btexpXML.setEnabled(D);
                botones.btimpXML.setEnabled(D);
                break;
        }
    }*/
