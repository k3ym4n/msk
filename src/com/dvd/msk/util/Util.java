package com.dvd.msk.util;

import javax.swing.*;

import static javax.swing.JOptionPane.*;

/**
 * Created by k3ym4n on 03/12/2015.
 */
public class Util {

    public static int borrarConfirmacion(String mensajeborrar){
        return showConfirmDialog(null, mensajeborrar, "Confirmar", YES_NO_OPTION);
    }

    public static void coherenciadefechas(String mensajeFecha,String Titulo){
         JOptionPane.showMessageDialog(null, mensajeFecha, Titulo, JOptionPane.INFORMATION_MESSAGE);
    }

    public static void campoObligatorio(String mensajeNumero,String Titulo){
        JOptionPane.showMessageDialog(null, mensajeNumero, Titulo, JOptionPane.INFORMATION_MESSAGE);
    }

    public static boolean esNumero (String texto){
        return texto.matches("[-+]?\\d+(\\.\\d+)?");
    }

    //todo mensajes de confirmacion en la modificacion , y en las exporatciones e importaciones.
}
