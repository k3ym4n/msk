package com.dvd.msk.util;

import java.io.File;

/**
 * Created by k3ym4n on 07/12/2015.
 */
public class Constantes {

    public static final String PATHGrupos = System.getProperty("user.home") + File.separator + "Grupos.dat";
    public static final String PATHDiscos = System.getProperty("user.home") + File.separator + "Discos.dat";
    public static final String PATHCanciones = System.getProperty("user.home") + File.separator + "Canciones.dat";

    public static final String PATHXML = System.getProperty("user.home") + File.separator + "fichero.xml";


}
