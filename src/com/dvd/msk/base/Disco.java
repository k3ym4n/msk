package com.dvd.msk.base;

import javafx.scene.control.ComboBox;

import java.io.Serializable;

/**
 * Created by k3ym4n on 03/12/2015.
 */
public class Disco implements Serializable{

    private String titulo,productora;
    private float duracion,precio;
    private int numcanciones;
    private String nombreGrupo;

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getProductora() {
        return productora;
    }

    public void setProductora(String productora) {
        this.productora = productora;
    }

    public float getDuracion() {
        return duracion;
    }

    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getNumcanciones() {
        return numcanciones;
    }

    public void setNumcanciones(int numcanciones) {
        this.numcanciones = numcanciones;
    }


    @Override
    public String toString() {
        return "Disco{" +
                "titulo='" + titulo + '\'' +
                ", productora='" + productora + '\'' +
                ", duracion=" + duracion +
                ", precio=" + precio +
                ", numcanciones=" + numcanciones +
                ", nombreGrupo='" + nombreGrupo + '\'' +
                '}';
    }
}
