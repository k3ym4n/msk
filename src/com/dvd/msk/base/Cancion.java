package com.dvd.msk.base;

import javafx.scene.control.ComboBox;

import java.io.Serializable;

/**
 * Created by k3ym4n on 03/12/2015.
 */
public class Cancion implements Serializable{

    private String titulo,genero;
    private float duracion,precio;
    private int track;

    private String nombreGrupo;
    private String nombreDisco;

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public String getNombreDisco() {
        return nombreDisco;
    }

    public void setNombreDisco(String nombreDisco) {
        this.nombreDisco = nombreDisco;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public float getDuracion() {
        return duracion;
    }

    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getTrack() {
        return track;
    }

    public void setTrack(int track) {
        this.track = track;
    }


    @Override
    public String toString() {
        return "Cancion{" +
                "titulo='" + titulo + '\'' +
                ", genero='" + genero + '\'' +
                ", duracion=" + duracion +
                ", precio=" + precio +
                ", track=" + track +
                ", nombreGrupo='" + nombreGrupo + '\'' +
                ", nombreDisco='" + nombreDisco + '\'' +
                '}';
    }
}
